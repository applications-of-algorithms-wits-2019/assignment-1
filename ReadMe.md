# Assignment 1 - Recursive algorithms

# Find max sub array

## Driver.java

Leaving everything in place - Run this to execute the experiment. 
</n> 
You will be prompted to enter two values to customize the array creation. I recommend using 5 as the first and 1000 as the second - these determine the length of the arrays and the range of the random integers. 
</n>
</n>
## Supporting Classes 
- Brute_Force.java
- Div_and_conq.java
- Custom.java
- LinearTime_Kadane.java

</n> All these classes contain functions are required to run Driver.java 
</n>
They all contain their own main function and can be run independently for inspection 
</n> 
</n>

## Testing Classes 
These classes are not necessary, but they have println probes as it were to trace variables through the process 

## Sketches 
These are just rough sketches and brain storming 