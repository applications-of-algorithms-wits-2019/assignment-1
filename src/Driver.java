import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Scanner;
import java.util.Random;
import java.lang.Math;

//TODO investigate 'warming up' the program - potential for decreased overhead once alreadly loaded into memory
public class Driver {
    public static void main(String[] args) throws IOException {
        //create file to save results
        LocalDateTime now = LocalDateTime.now();
        FileWriter csvWriter = new FileWriter("Results_"+now+".csv");
        csvWriter.append("Type");
        csvWriter.append(",");
        csvWriter.append("ArraySize");
        csvWriter.append(",");
        csvWriter.append("Elapse_Time");
        csvWriter.append("\n");

        //Intro//
        System.out.println("Welcome to the MaxSubArray solver");
        Scanner input = new Scanner(System. in);
        System.out.println("How long would you like to make the set of 5 arrays? <enter a base> e.g. b^2,b^4,b^8,b^16");
        int b = input. nextInt();
        System.out.println("What range should random elements be generated?");
        int range = input.nextInt();

        Arr[] arr_arr = makeArrs(b,range);
        int numOfArr = 5;


        ////////////// Kadane - LinearTime //////////////
        //output for user
        System.out.println("***** Linear Time solution - Using Kadane's algorithm *****");
        int[] arr = arr_arr[0].oneD;
        System.out.println("Initial array");
        for (int a = 0; a<arr.length; a++) System.out.print(arr[a] + " "); //print array
        //TODO add count to illustrate / graph step against time as well??
        //start timer
        long startTime = System.nanoTime();
        //Find max subarray
        LinearTime_Kadane.Result r = LinearTime_Kadane.findMaxSubArr_Lin_Kadane(arr); //textbook uses 1 indexing and java uses 0 indexing for arrays
        //end timer
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        //print results
        System.out.println("\n \nmax_i: "+ r.first + " max_j: " + r.second);
        System.out.println("Max value of sub-array: "+ r.third);
        System.out.println("Max sub-array: ");
        for (int i = r.first; i <= r.second; i++) System.out.print(arr[i] + " ");
        System.out.println("\nTime elapsed: " + timeElapsed/1000 + " microseconds");

        // computation for results
        int [] arrLengths = new int[numOfArr];
        for (int i = 0; i <numOfArr-1 ; i++) {
            arr = arr_arr[i].oneD;
            arrLengths[i] = arr.length;

            //START OF RUN x5 for each length of array
            int numRuns = 5;
            int timeElapsedAcc = 0;
            for (int runs = 0; runs < numRuns; runs++) {
                //start timer
                startTime = System.nanoTime();
                //Find max subarray
                r = LinearTime_Kadane.findMaxSubArr_Lin_Kadane(arr); //textbook uses 1 indexing and java uses 0 indexing for arrays
                //end timer
                endTime = System.nanoTime();
                timeElapsed = endTime - startTime;
                timeElapsedAcc += timeElapsed; //time accumulator for averaging
        }//END OF RUN

            //save results for array of specified length and type - to .csv file
            csvWriter.append("Kadane");
            csvWriter.append(",");
            csvWriter.append(Integer.toString(arr.length));
            csvWriter.append(",");
            csvWriter.append(Integer.toString(timeElapsedAcc/1000/numRuns));
            csvWriter.append("\n");

            System.out.println("\nAverage Time elapsed: " + timeElapsedAcc/1000/numRuns + " microseconds");
        }

        ////////////// Custom - Attempted LinearTime //////////////
        //output for user
        System.out.println("***** Attempted Linear Time custom solution *****");
        arr = arr_arr[0].oneD;
        System.out.println("Initial array");
        for (int a = 0; a<arr.length; a++) System.out.print(arr[a] + " "); //print array
        //start timer
        startTime = System.nanoTime();
        //Find max subarray
        Custom.Result r_LT = Custom.findMaxSubArr_Lin(arr); //textbook uses 1 indexing and java uses 0 indexing for arrays
        //end timer
        endTime = System.nanoTime();
        timeElapsed = endTime - startTime;
        //print results
        System.out.println("\n \nmax_i: "+ r_LT.first + " max_j: " + r_LT.second);
        System.out.println("Max value of sub-array: "+ r_LT.third);
        System.out.println("Max sub-array: ");
        for (int i = r_LT.first; i <= r_LT.second; i++) System.out.print(arr[i] + " ");
        System.out.println("\nTime elapsed: " + timeElapsed/1000 + " microseconds");

        // computation for results
        arrLengths = new int[numOfArr];
        for (int i = 0; i <numOfArr-1 ; i++) {
            arr = arr_arr[i].oneD;
            arrLengths[i] = arr.length;

            //START OF RUN x5 for each length of array
            int numRuns = 5;
            int timeElapsedAcc = 0;
            for (int runs = 0; runs < numRuns; runs++) {
                //start timer
                startTime = System.nanoTime();
                //Find max subarray
                r_LT = Custom.findMaxSubArr_Lin(arr); //textbook uses 1 indexing and java uses 0 indexing for arrays
                //end timer
                endTime = System.nanoTime();
                timeElapsed = endTime - startTime;
                timeElapsedAcc += timeElapsed; //time accumulator for averaging
            }//END OF RUN

            //save results for array of specified length and type - to .csv file
            csvWriter.append("Custom");
            csvWriter.append(",");
            csvWriter.append(Integer.toString(arr.length));
            csvWriter.append(",");
            csvWriter.append(Integer.toString(timeElapsedAcc/1000/numRuns));
            csvWriter.append("\n");

            System.out.println("\nAverage Time elapsed: " + timeElapsedAcc/1000/numRuns + " microseconds");
        }

        ////////////// Divide and Conquer //////////////
        //output for user
        System.out.println("***** Divide and Conquer *****");
        arr = arr_arr[0].oneD;
        System.out.println("Initial array");
        for (int a = 0; a<arr.length; a++) System.out.print(arr[a] + " "); //print array
        //start timer
        startTime = System.nanoTime();
        //Find max subarray
        Div_and_conq.Result r_DC = Div_and_conq.findMaxSubArr_DAC(arr, 0, arr.length-1); //textbook uses 1 indexing and java uses 0 indexing for arrays
        //end timer
        endTime = System.nanoTime();
        timeElapsed = endTime - startTime;
        //print results
        System.out.println("\n \nmax_i: "+ r_DC.first + " max_j: " + r_DC.second);
        System.out.println("Max value of sub-array: "+ r_DC.third);
        System.out.println("Max sub-array: ");
        for (int i = r_DC.first; i <= r_DC.second; i++) System.out.print(arr[i] + " ");
        System.out.println("\n Time elapsed: " + timeElapsed/1000 + " microseconds");

        // computation for results
        arrLengths = new int[numOfArr];
        for (int i = 0; i <numOfArr-1 ; i++) {
            arr = arr_arr[i].oneD;
            arrLengths[i] = arr.length;

            //START OF RUN x5 for each length of array
            int numRuns = 5;
            int timeElapsedAcc = 0;
            for (int runs = 0; runs < numRuns; runs++) {
                //start timer
                startTime = System.nanoTime();
                //Find max subarray
                r_DC = Div_and_conq.findMaxSubArr_DAC(arr, 0, arr.length-1); //textbook uses 1 indexing and java uses 0 indexing for arrays
                //end timer
                endTime = System.nanoTime();
                timeElapsed = endTime - startTime;
                timeElapsedAcc += timeElapsed; //time accumulator for averaging
            }//END OF RUN

            //save results for array of specified length and type - to .csv file
            csvWriter.append("Div&Conq");
            csvWriter.append(",");
            csvWriter.append(Integer.toString(arr.length));
            csvWriter.append(",");
            csvWriter.append(Integer.toString(timeElapsedAcc/1000/numRuns));
            csvWriter.append("\n");

            System.out.println("\nAverage Time elapsed: " + timeElapsedAcc/1000/numRuns + " microseconds");
        }

        ////////////// BRUTE FORCE //////////////
        //output for user
        System.out.println("***** BRUTE FORCE *****");
        arr = arr_arr[0].oneD;
        System.out.println("Initial array");
        for (int a = 0; a<arr.length; a++) System.out.print(arr[a] + " "); //print array
        //start timer
        startTime = System.nanoTime();
        //Find max subarray
        Brute_Force.Result r_BF = Brute_Force.findMaxSubArr_BF(arr); //textbook uses 1 indexing and java uses 0 indexing for arrays
        //end timer
        endTime = System.nanoTime();
        timeElapsed = endTime - startTime;
        //print results
        System.out.println("\n \nmax_i: "+ r_BF.first + " max_j: " + r_BF.second);
        System.out.println("Max value of sub-array: "+ r_BF.third);
        System.out.println("Max sub-array: ");
        for (int i = r_BF.first; i <= r_BF.second; i++) System.out.print(arr[i] + " ");
        System.out.println("\n Time elapsed: " + timeElapsed/1000 + " microseconds");

        // computation for results
        arrLengths = new int[numOfArr];
        for (int i = 0; i <numOfArr-1 ; i++) {
            arr = arr_arr[i].oneD;
            arrLengths[i] = arr.length;

            //START OF RUN x5 for each length of array
            int numRuns = 5;
            int timeElapsedAcc = 0;
            for (int runs = 0; runs < numRuns; runs++) {
                //start timer
                startTime = System.nanoTime();
                //Find max subarray
                r_BF = Brute_Force.findMaxSubArr_BF(arr); //textbook uses 1 indexing and java uses 0 indexing for arrays
                //end timer
                endTime = System.nanoTime();
                timeElapsed = endTime - startTime;
                timeElapsedAcc += timeElapsed; //time accumulator for averaging
            }//END OF RUN

            //save results for array of specified length and type - to .csv file
            csvWriter.append("BRUTE_FORCE");
            csvWriter.append(",");
            csvWriter.append(Integer.toString(arr.length));
            csvWriter.append(",");
            csvWriter.append(Integer.toString(timeElapsedAcc/1000/numRuns));
            csvWriter.append("\n");

            System.out.println("\nAverage Time elapsed: " + timeElapsedAcc/1000/numRuns + " microseconds");
        }

        //save results - send to file and close
        csvWriter.flush();
        csvWriter.close();

        ///////////////
        //END OF MAIN//
        ///////////////
    }

    //class to hold Array of Arrays
    static final class Arr {
        public final int [] oneD;

        public Arr(int len) {
            this.oneD = new int[len];
        }
    }

    //function to create array of random arrays that vary in length incrementally
    public static Arr[] makeArrs (int b, int range) {
        int [] lenArr = {(int)Math.pow(b,2),(int)Math.pow(b,3.5),(int)Math.pow(b,4),(int)Math.pow(b,4.5),(int)Math.pow(b,1)};
        int numOfArr = 6;
        Arr[] arr_arr = new Arr[numOfArr+1];
        Random rand = new Random();

        for (int i = 0; i < numOfArr-1 ; i++) { //FIXME can't cyle through all arrays - getting out of bounds error
            arr_arr[i] = new Arr(lenArr[i]);
            for (int j = 0; j < lenArr[i]; j++) { //make random array of length b^x
                arr_arr[i].oneD[j] = rand.nextInt(range)-range/2;
            }
        }
        return arr_arr;
    }

}
