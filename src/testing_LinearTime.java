import java.lang.Math; // for Math.floor()
import java.util.concurrent.TimeUnit; // for timer

public class testing_LinearTime {

    public static void main(String[] args) {
        System.out.println("Linear Time solution");

        //TODO make randomized array of between some two values -'ve and +'ve of incramental length through runs

        int[] arr = {13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7};
//        int[] arr = {-13, -3, -25, -20, -3, -16, -23, -18, -20, -7, -12, -5, 22, 15, -4, 7};
        System.out.println("Initial array");
        for (int a = 0; a<arr.length; a++) System.out.print(arr[a] + " "); //print array

        //TODO add for loop to get average time over multiple runs and output to file in csv format for plotting
        //TODO add count to illustrate / graph step against time as well??
        //start timer
        long startTime = System.nanoTime();
        //Find max subarray
        Result r = findMaxSubArr_Lin(arr); //textbook uses 1 indexing and java uses 0 indexing for arrays

        //end timer
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;

        //print results
        System.out.println("\nmax_i: "+ r.first + " max_j: " + r.second);
        System.out.println("Max value of sub-array: "+ r.third);
        System.out.println("Max sub-array: ");
        for (int i = r.first; i <= r.second; i++) System.out.print(arr[i] + " ");
        System.out.println("\n \nTime elapsed: " + timeElapsed/1000 + " microseconds");
    }


    public static Result findMaxSubArr_Lin (int[] arr){
        //initialize variables
        int max = 0;
        int sum_j = 0;
        int sum_jp1 = arr[0];

        int sum_T = 0;
        int max_i = 0; // 1 in pseudo code
        int max_j = 0; // 1 in pseudo code

        // for (n) + for ( < n)
        for (int j = 0; j < arr.length -1 ; j++) { //double check indexing

            System.out.println("\n -"+ j +"-  *** " + arr[j] + " ***");

            sum_j += arr[j];
            sum_jp1 += arr[j+1];

            System.out.println("\nsum_j: " + sum_j);
            System.out.println("sum_jp1: " + sum_jp1);

            if (sum_j > max) {
                max = sum_j;
                max_j = j;

                System.out.println("max:" + max);
                System.out.println("max_j: " + max_j);
            }
            if (sum_jp1 < sum_j) { //this works but is it right ??
                sum_T = sum_j;
                for (int i = 0; i <= j; i++) { //make sure this is correctly indexed i.e. j-1 ? //testing the either ... or
                    sum_T = sum_T - arr[i]; //problem here
                    for (int a = i ; a <= j; a++) System.out.print(arr[a] + " ");
                    System.out.println("\nsum_T: " + sum_T);
                    if (sum_T > max) {
                        max = sum_T;
                        System.out.println(" --max: " + max);
                        max_i = i;
                        max_j = j;
                        System.out.println(" --max_i: " + max_i);
                        System.out.println(" --max_j: " + max_j);
                    }
                }
            }
        }
        return new Result (max_i, max_j, max);
    }




    public static int sumSubArr (int i, int j,int[] arr) {
        int sum = 0;
        while (i <= j) {
            sum += arr[i];
            i++;
        }
        return sum ;
    }


    static final class Result {
        private final int first;
        private final int second;
        private final int third;

        public Result(int first, int second, int third) {
            this.first = first;
            this.second = second;
            this.third = third;
        }
    }

}

