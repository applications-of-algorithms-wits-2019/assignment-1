//sources
//https://en.wikipedia.org/wiki/Maximum_subarray_problem
//https://www.algorithmist.com/index.php/Kadane's_Algorithm
//https://www.geeksforgeeks.org/largest-sum-contiguous-subarray/

import java.lang.Math; // for Math.floor()
import java.util.concurrent.TimeUnit; // for timer

public class LinearTime_Kadane {

    public static void main(String[] args) {
        System.out.println("Linear Time solution - Using Kadane's algorithm");

        //TODO make randomized array of between some two values -'ve and +'ve of incramental length through runs

        int[] arr = {13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7};
//        int[] arr = {-13, -3, -25, -20, -3, -16, -23, -18, -20, -7, -12, -5, 22, 15, -4, 7};
        System.out.println("Initial array");
        for (int a = 0; a<arr.length; a++) System.out.print(arr[a] + " "); //print array

        //TODO add for loop to get average time over multiple runs and output to file in csv format for plotting
        //TODO add count to illustrate / graph step against time as well??
        //start timer
        long startTime = System.nanoTime();
        //Find max subarray
        Result r = findMaxSubArr_Lin_Kadane(arr); //textbook uses 1 indexing and java uses 0 indexing for arrays
        //end timer
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;

        //print results
        System.out.println("\n \nmax_i: "+ r.first + " max_j: " + r.second);
        System.out.println("Max value of sub-array: "+ r.third);
        System.out.println("Max sub-array: ");
        for (int i = r.first; i <= r.second; i++) System.out.print(arr[i] + " ");
        System.out.println("\n \nTime elapsed: " + timeElapsed/1000 + " microseconds");
    }


    public static Result findMaxSubArr_Lin_Kadane (int[] arr){
        //initialize variables
        int max_so_far = Integer.MIN_VALUE,
                max_ending_here = 0
                ,start_i = 0, end_i = 0, s = 0;

        for (int i = 0; i < arr.length; i++){
            max_ending_here += arr[i];
            if (max_so_far < max_ending_here){
                max_so_far = max_ending_here;
                start_i = s;
                end_i = i;
            }
            if (max_ending_here < 0){
                max_ending_here = 0;
                s = i + 1;
            }
        }
        return new Result (start_i, end_i, max_so_far);
    }


    public static int sumSubArr (int i, int j,int[] arr) {
        int sum = 0;
        while (i <= j) {
            sum += arr[i];
            i++;
        }
        return sum ;
    }


    static final class Result {
        public final int first;
        public final int second;
        public final int third;

        public Result(int first, int second, int third) {
            this.first = first;
            this.second = second;
            this.third = third;
        }
    }

}

