import java.util.concurrent.TimeUnit; // for timer

public class Brute_Force {

    public static void main(String[] args) {
        System.out.println("Brute-Force solution");
        int[] arr = {13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7};

        System.out.println("Initial array");
        for (int a = 0; a<arr.length; a++) System.out.print(arr[a] + " "); //print array

        //TODO add for loop to get average time over multiple runs and output to file in csv format for plotting
        //start timer
        long startTime = System.nanoTime();
        //Find max subarray
        Result r = findMaxSubArr_BF(arr);
        //end timer
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;

        //print results
//        System.out.println("\n \n" + "Max value of sub array is: " + max);
//        System.out.print("The max sub array is: ");
//        for (int i = index_1; i <= index_2; i++) System.out.print(arr[i] + " ");
//        System.out.println("\n \nTime elapsed: " + timeElapsed/1000 + " microseconds");
    }

    public static Result findMaxSubArr_BF(int[] arr) {
        int max = 0;
        int index_1 = 0;
        int index_2 = 0;
        for (int i = 0; i<arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (sumSubArr(i,j, arr) > max) {
                    max = sumSubArr(i,j,arr);
                    index_1 = i;
                    index_2 = j;
                }
            }
        }
        return new Result(index_1, index_2, max);
    }

    public static int sumSubArr (int i, int j,int[] arr) {
        int sum = 0;
        while (i <= j) {
            sum += arr[i];
            i++;
        }
        return sum ;
    }

    static final class Result {
        public final int first;
        public final int second;
        public final int third;

        public Result(int first, int second, int third) {
            this.first = first;
            this.second = second;
            this.third = third;
        }
    }
}

